
# ENL-Encoded_Natural_Language

---

![Example Image](/static/images/enl-logo.jpg)

---

**Example-Syntax**

+---

!Note:[2019-11-2:4pm, {tag:[enl,enccoded natural language]}, {pn: 'enl-notes'}]
!@?ENL?&Encoded*Natural*Language$;^
ENL?&Encoded*Natural*Language$&:<
!:Example$&:<
!#research-computers?&programming:$_transpiling;^
!:example*ml-mnemonic*syntax&:<

/:(

!  +n?(command)followed by a 'sub-symbol soft-command': - sub&symbol&soft&commands:[ #, @, @?, :, <, ? ]  -- is an ENL-Imperative-command: if followed by one of the
!<?given 'sub-symbol-soft-commands' makes the ENL-Imperative-command into a soft and therefore, a more informed command&F>(further-processing)%type>command.  (!:! ;^)

More:
&:<

### ;-_
statement terminator / instruction&:<(end-cmd-seq)\-  reg expression: ( regex pattern match: /(;\^)\s*\n*/gu ) example: https://regexr.com/4o2dj

### \\-
(enl.end-cmd-soft) soft statement terminator  regexp:  https://regexr.com/4o2k2

### [=
denotes: other encoded/mnemonic/mark-down/descriptor for  processing following after;   reg expression: ( regex pattern match: /(\[=)[\p{L}\p{Nd}\W]*(;-_)/gu ) example: https://regexr.com/4o2f9

### !N:>
denotes: a note of text following after until enl.end-cmd-seq ( ;-_ ) is found.  reg expression: ( regex pattern match: /(!note>)[\p{L}\p{Nd}\W]*(;-_)/gu ) example:  https://regexr.com/4o2jp

### /!{
(enl.enl-escape) denotes: ENL escape / break out of ENL / process as (do identical) / and (no ENL format) / literal text / following after: reg expression: ( regex pattern match: /(/:\()/gu ) example: https://regexr.com/4o2cu

### &:>
(enl-and-more || and-more) denotes: (extra content related to 'previous-command-text' preceding before:  reg expression: ( regex pattern match: /(&:<)\s*\n*/gu ) example:  https://regexr.com/4o2e8

### !#<tagname>:>
(enl.enl-subject-command) -
;---

---
