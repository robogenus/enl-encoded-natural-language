#ENL - Encoded Natural Language Development Notes:

##ENL-DevNote: 11-15-2019-10:30:00:

---
!# note +:>
!=# enlnotes +:>
!@incl:>!:{
[
  "src":
  {
    "idea":"Spaces Are Code",
    "reserve":"?",
    "class":"enl-sub-format-src",
    "acronym":"SRC",
    "description":"Whitespace type characters in encoded documents can be use in a way (with careful formatting and arrangement) to structure hierarchical relational data, much like XML and JSON, and can be parsed rapidly.",
    "notes":
    {
      "1":"  ...(?)further notes   ",
    }
  }
]
;-_

---

+---

**ENL System Versioning** is to use this: ```(**enl.cmd.vers.std**):**version** = **1A0**``` thus explained and defined by the following:

**[**
  The version extension value generally for most software is formatted like <SomeApp>.version=1.0.0 (current conventional standard). ENL is to deviate from the standard by being abbreviated using alpha-numeric type format schema (*representing a 3 part hexadecimal identifying type versioning numeric value*) and the initial version alpha-numeric value should start at **`1A0`**. Thus, this format expresses a 3 part versioning identifying value, with each part representing a hexadecimal number. An example version value format is constructed like so in the following notes:

  --->  **note!** the (**+** *plus sign*) in the description below represents concatenation between each version value part in { braces } of the complete version value  <---

  Initial Version Value **`1A0`** or **`1a0`** -- description and explanation of construction below:

  { the common-version-extension-dot ( **.** ) } **+**

  { the **first** part hexadecimal number **`1`** is the represented version value for the **first initial** version of all versions following and will be the root (*long-term*) hexadecimal numeric version value that should only be incremented after many sub-versions have been developed, tested, and released, (representing a major release version) and the value is the hexadecimal number **`0x1`** with capability to be incremented, albeit should generally stay small by convention } **+**

  { the next **`A`** letter part of the version value is the **second** hexadecimal value that will always start with **`0xA`** equating to **`10`** *in base10* for the **initial** application version and should generally not be incremented higher than **`0xFF`** (**`FF`**) equating to **`255`** *in base10* for any sub-versions } **+**

  { the third hexadecimal part of the version value, in this example being **`0`**, is used for quick bug-fix, patches, and small to moderate version differences and should again, by convention, not be incremented higher that **`0x99`** (**`99`**) in hexadecimal equated to **`153`** *in base10* }.

  This versioning format allows for **`39015`** (*base10*) possible sub-version permutations from the **(*)Product** of multiplying the *last two parts* (*maxed values*) of the version value, the *second part* **`0xA`**(**`A`**) maxed to **`0xFF`**(**`FF`**) and the *last part* **`0`** maxed to **`0x99`**(**`99`**) and not including the **first** hexadecimal value. Therefore, version example with maxed value **`.1FF99`** equates to **`1`** `+` **`0xFF`**:(**`255`** *base10*) `x` **`0x99`**:(**`153`** *base10*) `=` **`39016`** (*base10*) different variations of versions, which should be, in most cases, more than enough variation values to represent different versions, and thus, making the version value to be represented without the **dot** (**`.`**) separators.
**]**

;---

---

+---

--->
**Important!** *** [ a *series of 3* (**-**) *dashes in a row and one* (**>**) *greater-than-arrow* **`--->`  or `!x:>`** ] denotes: an enl human scratch note and  the **`enl.sys`** (*enl system*) interprets this as (enl.cmd.scratch) and a scratch-note should be terminated with *one* (**<**) *less-than-arrow and a series of 3* (**-**) *dashes in a row* **`<---`** -- This can be consider ENL's syntax for human *Comments* -- (?)Possibly in enl.future.versions this command will permu-morph into enl.sys.cmds.scratch. You will encounter examples of this throughout the following notes  

**Note! `+---`** is an `enl-opening-section-separator` and section data should be closed with an
`enl-ending-section-separator` **`;---`**

**`!x:>`** (*enl.sys.cmds.no-parse*) **`enl.sys`** interprets this command as *Ignore following Data on Parsing document data into human readable format. This Data included should be terminate with a* **`;-_`** (**`enl.sys.cmds.term-seq`**) --- *enl-terminate-sequence-command*. Human readable format means that all the enl encoding is removed.

(`enl.sys.cmds.like`) **`!like#`** or short-form **`!=#`** denotes: *that this data-member-element { ex: !=#(SomeDataElementText):> } will be interpreted by* **`enl.sys`** *as an alias-data-member-element to the previous data-member-element and the aliases will be related and link to all the following sub-data-members data*. This instruction command can be chained.

**`!?:>`** (`enl.sys.cmds.idea`) **`enl.sys`** interprets this as the following data will be defined as a human idea that is related to the direct preceding command statement and the idea data should be terminated with a **`;-_`** (`enl.sys.cmds.term-seq`) *enl-command-sequence-terminator*.

**`<!!`** (`enl.cmd.pre-incomplete`) **`enl.sys`** interprets this as: *the previous statements are to be interpreted as incomplete and are to be (?)possibly indexed and/or kept reserved for user recall reminders list of incomplete or unfinished data, this will allow for user to recall all incomplete data so that user may at some point continue to complete the data at a later time*.

(?)consider: (`enl.sys.cmds.def`) **`!def:>`** or short-form (**`!d:>`**) or long-form (**`!define:>`**) -- enl syntax instruction command **`enl.sys`** interprets this command as: *all of the following data-member-elements will relate and describe/define the data-member-element that precedes this command. Example Structure* **`!#**(**member-element**)**:>`** *This enables a syntactic structuring of hierarchical-relevant-relationships*

(?)consider: **`!^`** (`env.sys.cmds.dscr-note`) **`enl.sys`** interprets as the following statements are to be *description-notes* for the previous *data-member-element*.

(?)consider: **`!ep#`** (`env.sys.cmds.create-env`) **`enl.sys`** interprets as to create a *Global-Environmental-Property*. Command syntax format: `!ep#(DataMemberText):>` ... the *DataMemberText* will be assigned as the *key* and the next statement following this command, until another command is found, will be assigned as the properties *value*.
<---

;---

---

+---

--->  ENL Data-Member-Elements Relationships & Associations  <---
```
!# enl-elements-relations +:>
```

--->  
*the following statements are defining some 'aliases' for the above previous statement*
<---

```
!like# enl.mbr.rels +:>
!=# enl.mbr.assoc +:>
!=# enl.data.rels +:>
!=# enl-elems-rels +:>
!like# enl-elems-assoc +:>
!=# enl-elems-associations +:>
```

----->  
        Remember and consider these conceptual ideas towards further development:

        1> !?:> member-elements should be able to relate not only to a parent-type member as a child-type relative, but should be a flexible model so that the member-element can develop or (?)un-develop an associated relationship with another member and that relationship should remain flexible and towards each entity having a mutable role-of-seniority as an enl.parser.ml-engine determines hierarchical-importance-measure value. Think one-to-many, many-to-one. ;-_

        2> created member-element Aggregator
        3> member-element with extended persistence will grow and therefore there needs to be a data-cleaner-scrubber which will most likely employ some sort of data-validation-processing  
<-----
