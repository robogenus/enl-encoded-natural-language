# ENL - Encoded Natural Language   
---   
![ENL](/static/images/enl-logo.jpg)

[View ENL Logo](https://crello.com/design/5dca030b13bd5e006faef527-94bf2-enl-logo/)

---

**ENL Original Author and Developer:**  
Donovan Edwards   
`robogenus@GmailDotCom`

## About ENL:

**ENL - Encoded Natural Language** is being developed to be a mnemonic markup syntax for describing, processing, and presenting textual data. Further, it is also to be a light weight and powerful interface of encoded natural language for NLP - natural language processing. ENL is to be a method for encoding natural human language by writing or speaking ENL commands to encode natural language. The ENL commands are predefined utf-8 character sequences that can often be chained commands and presented as named 'ENL Syntax Commands'. ENL, as developed, should attempt to adhere to some of the following goals:

- ***Open-Source and as a standard***
- ***Easy to understand and very human readable***
- ***Can easily mesh with other markup such as Markdown, XML, JSON***
- ***Should be novel, light-weight, high-level***
- ***logical with capable abstraction, cross-compatible***
- ***customize-able and flexible with high degree of utility functionality***
- ***Standardized for cross-communication, cross-system AI(NLP) communication interfacing***
- ***Human and AI(NLP) Communication Interfacing, Interpretation, and Understanding***

---

## Help Develop ENL:  

**The ENL Project is Looking For Contributors**

The ***ENL Project*** is open-source and is in need of your support if you are interested and enjoy being an open-source contributor. The ENL development team really needs some very creative project coordinators, technical writers, computer programmers, engineers, and scientists, advanced brainy math wizards :) and other suggested.

The ENL Project is about communication, which in accordance with, means that anyone that contributes should try to conduct a professional and respectful relationship with any other ENL Project contributors. The ENL main project contributors reserve the right to remove any individual contributor from the main ENL contributors group via a 'Consensus-Minus-One' decision making process, if they feel it is in the best interest of ENL's development. The Main ENL Branch Project Contributors Team is further defined and managed as a **'Consensus-Minus-One'**. Thus, meaning that periodically there shall be a consensus vote about certain processes, development goals, regulations, standards, and other type properties, methods and operations concerning the development of the ENL Project.

If you are interested in being an active contributor and participating as a developer, other type contributor, or supporter of the ENL Project, we sure could use your help. The current contributors would be very excited and will truly appreciate to welcome you aboard!

If all this describes you then please reach out and contact the current project coordinator at the email below and, if you don't mind, include **`!#ENL`** as the first part of the subject line in an email. Also, please mention how you can help to contribute to the ENL project and any other comments or info that you would like to present in the email body.

Soon after the current project coordinator receives your email, they will present it to the active contributors. The active contributors group will then elect to have someone contact you for an informal meeting and introductory conversation. The meeting's context will be to ascertain and decide what focus role might best be ascribed to you, accordingly.

The meeting most likely will be conducted via phone, Skype, Slack, Discord or other. After, and in a timely manner, the active ENL contributors group will perform a 'Consensus-Minus-One' vote to induct you in as an active ENL Project group member. Then, the project coordinator will contact you to setup another meeting to coordinate with you and discuss any other necessary details to help get you started in your ENL Project involvement. We are so excited that you will be contributing and we want to **'Thank You'** in advance.

Please address and send your email to:  

**Current ENL Project Coordinator:**   
Donovan Edwards   
`robogenus@GmailDotCom`

---

**ENL - Encoded Natural Language Open Source License Type**

**GNU Affero General Public License v3.0**

-- Project License Details Are Found In the [*License.txt*](https://bitbucket.org/robogenus/enl-encoded-natural-language/src/master/License.txt) File residing in the root directory.
-- Alternate Link: [*License.txt*](https://www.gnu.org/licenses/agpl-3.0.txt)

---
